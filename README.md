insert\_suspend\_hlsearch.vim
=============================

This plugin quietly disables `'hlsearch'` search highlighting if enabled when
an insert operation is started, and puts it back once done, to avoid the
distracting effect the highlighting can cause while writing.

Requires Vim 7.0 or later.

License
-------

Copyright (c) [Tom Ryder][1].  Distributed under the same terms as Vim itself.
See `:help license`.

[1]: https://sanctum.geek.nz/
