"
" insert_suspend_hlsearch.vim: If 'hlsearch' is enabled, switch it off when
" the user starts an insert mode operation, and back on again when they're
" done.
"
" Author: Tom Ryder <tom@sanctum.geek.nz>
" License: Same as Vim itself
"
if exists('loaded_insert_suspend_hlsearch') || &compatible && v:version < 700
  finish
endif
let loaded_insert_suspend_hlsearch = 1

" Clear search highlighting as soon as I enter insert mode, and restore it
" once left
augroup insert_suspend_hlsearch
  autocmd!
  autocmd InsertEnter *
        \ call insert_suspend_hlsearch#()
augroup END
