" Suspend 'hlsearch' if set, add hook to restore on insert completion
function! insert_suspend_hlsearch#() abort

  " Nothing to do if 'hlsearch' isn't set
  if !&hlsearch
    return
  endif

  " Turn off 'hlsearch' and set a hook to re-enable it when insert mode ends
  set nohlsearch
  autocmd insert_suspend_hlsearch InsertLeave *
        \ set hlsearch
        \|autocmd! insert_suspend_hlsearch InsertLeave

endfunction
