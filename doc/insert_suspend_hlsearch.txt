*insert_suspend_hlsearch.txt*	For Vim version 7.0	Last change: 2019 May 25

DESCRIPTION					*insert_suspend_hlsearch*

This plugin disables 'hlsearch' search highlighting if enabled when an insert
operation is started, and puts it back once done, to avoid the distracting
effect the highlighting can cause while writing.  Its implementation is a
crude workaround for the |:nohlsearch| limitations described in
|autocmd-searchpat|.

REQUIREMENTS					*insert_suspend_hlsearch-requirements*

This plugin is only available if 'compatible' is not set.

AUTHOR						*insert_suspend_hlsearch-author*

Written and maintained by Tom Ryder <tom@sanctum.geek.nz>.

LICENSE						*insert_suspend_hlsearch-license*

Licensed for distribution under the same terms as Vim itself (see |license|).

 vim:tw=78:ts=8:ft=help:norl:
